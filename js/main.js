const text = /^[0-9a-zA-Z]+$/;
const passChar = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/;
const number = /^[0-9]+$/;
let numberRnd;
const user = {};
const help = document.getElementsByClassName('help');
const userName = document.getElementById('usernameId');
const codeP = document.getElementById('codeId');
const tel = document.getElementById('telId');
const password = document.getElementById('passId');
const cPassword = document.getElementById('cPassId');
const captchaTxt = document.getElementById('captchaId');

function gCaptcha() {
  numberRnd = Math.floor(Math.random() * 10000);
  document.getElementById('captcha').innerHTML = numberRnd;
}
gCaptcha();
function message(numberP, targetEl, msg) {
  targetEl.classList.add('is-danger');
  targetEl.classList.remove('is-success');

  help[numberP].innerHTML = msg;
  help[numberP].classList.add('is-danger');
}
function chackUserName(value) {
  const targetEl = document.getElementById('usernameId');

  if (value.length === 0) {
    message(0, targetEl, 'خالی نباشد');
    return false;
  } if (value[0].match(number)) {
    message(0, targetEl, 'ابتدای نام کاربری نمی تواند عدد باشد');
    return false;
  } if (value.length < 5 || value.length > 25) {
    message(0, targetEl, ' نام کاربری نباید کمتر از 5 یا بیشتر از 25 کاراکتر باشد ');
    return false;
  } if (!(value.match(text))) {
    message(0, targetEl, 'لطفا از کاراکتر های مجاز برای نام کاربری استفاده نمایید');
    return false;
  }
  targetEl.classList.add('is-success');
  targetEl.classList.remove('is-danger');

  help[0].classList.remove('is-danger');
  help[0].innerHTML = '';
  return true;
}

function checkCaptcha(value) {
  const targetEl = document.getElementById('captchaId');
  if (value.length === 0) {
    message(5, targetEl, 'کد امنیتی صحیح نمی باشد');
    return false;
  } if (value !== numberRnd.toString()) {
    message(5, targetEl, 'کد امنیتی صحیح نمی باشد');
    return false;
  }
  targetEl.classList.add('is-success');
  targetEl.classList.remove('is-danger');
  help[5].classList.remove('is-danger');
  help[5].innerHTML = '';
  return true;
}

function checkConfirmPass(value, comValue) {
  const targetEl = document.getElementById('cPassId');
  if (comValue.length === 0) {
    message(4, targetEl, 'تکرار پسورد نباید خالی باشد');
    return false;
  }
  if (value !== comValue) {
    message(4, targetEl, 'تکرار پسورد نباید خالی باشد');
    return false;
  }
  targetEl.classList.add('is-success');
  targetEl.classList.remove('is-danger');
  help[4].classList.remove('is-danger');
  help[4].innerHTML = '';
  return true;
}

function checkPass(value) {
  const targetEl = document.getElementById('passId');
  if (value.length === 0) {
    message(3, targetEl, 'پسورد نباید خالی باشد');
    return false;
  } if (value.length < 8 || value.length > 25) {
    message(3, targetEl, 'پسورد نباید کمتر از 8 یا بیشتر از 25 کاراکتر باشد');
    return false;
  } if (!(value.match(passChar))) {
    message(3, targetEl, 'پسورد باید شامل عدد حروف حروف بزرگ باشد');
    return false;
  }
  targetEl.classList.add('is-success');
  targetEl.classList.remove('is-danger');
  help[3].classList.remove('is-danger');
  help[3].innerHTML = '';
  return true;
}

function checkCode(value) {
  const targetEl = document.getElementById('codeId');

  let validCode = false;
  let sum = 0;
  for (let i = 0; i < 9; i += 1) {
    sum += value[i] * (value.length - i);
  }

  if (((sum % 11) >= 2 && (11 - (sum % 11)) === parseInt(value[9], 10)) || ((sum % 11) < 2
    && (sum % 11) === parseInt(value[9], 10))) {
    validCode = true;
  }

  if (value.length === 0) {
    message(1, targetEl, 'کد ملی نباید خالی باشد');
    return false;
  } if (!validCode) {
    message(1, targetEl, 'کدملی نامعتبر است');
    return false;
  }
  targetEl.classList.add('is-success');
  targetEl.classList.remove('is-danger');
  help[1].classList.remove('is-danger');
  help[1].innerHTML = '';
  return true;
}

function checkTel(value) {
  const targetEl = document.getElementById('telId');
  if (value.length === 0) {
    message(2, targetEl, 'تلفن نباید خالی باشد');
    return false;
  } if (!(value.match(number))) {
    message(2, targetEl, 'لطفا از کاراکتر های مجاز برای تلفن استفاده نمایید');
    return false;
  } if (value[0] !== '0' || value[1] !== '9') {
    message(2, targetEl, 'لطفا شماره همراه را به درستی وارد نمایید');
    return false;
  } if (value.length > 11 || value.length < 11) {
    message(2, targetEl, 'لطفا شماره همراه را به درستی وارد نمایید');
    return false;
  }
  targetEl.classList.add('is-success');
  targetEl.classList.remove('is-danger');
  help[2].classList.remove('is-danger');
  help[2].innerHTML = '';
  return true;
}

function resetForm() {
  userName.classList.remove('is-success');
  userName.classList.remove('is-danger');
  help[0].classList.remove('is-danger');
  help[0].classList.remove('is-success');
  help[0].innerHTML = '';
  codeP.classList.remove('is-success');
  codeP.classList.remove('is-danger');
  help[1].classList.remove('is-danger');
  help[1].classList.remove('is-success');
  help[1].innerHTML = '';
  tel.classList.remove('is-success');
  tel.classList.remove('is-danger');
  help[2].classList.remove('is-danger');
  help[2].classList.remove('is-success');
  help[2].innerHTML = '';
  password.classList.remove('is-success');
  password.classList.remove('is-danger');
  help[3].classList.remove('is-danger');
  help[3].classList.remove('is-success');
  help[3].innerHTML = '';
  cPassword.classList.remove('is-success');
  cPassword.classList.remove('is-danger');
  help[4].classList.remove('is-danger');
  help[4].classList.remove('is-success');
  help[4].innerHTML = '';
  captchaTxt.classList.remove('is-success');
  captchaTxt.classList.remove('is-danger');
  help[5].classList.remove('is-danger');
  help[5].classList.remove('is-success');
  help[5].innerHTML = '';
  document.getElementById('usernameId').value = '';
  document.getElementById('codeId').value = '';
  document.getElementById('telId').value = '';
  document.getElementById('passId').value = '';
  document.getElementById('cPassId').value = '';
  document.getElementById('captchaId').value = '';
}

function checkValidation() {
  if (chackUserName(document.getElementById('usernameId').value) && checkCode(document.getElementById('codeId').value) && checkTel(document.getElementById('telId').value) && checkPass(document.getElementById('passId').value)
    && checkConfirmPass(document.getElementById('passId').value, document.getElementById('cPassId').value) && checkCaptcha(document.getElementById('captchaId').value)) {
    user.username = document.getElementById('usernameId').value;
    user.code = document.getElementById('codeId').value;
    user.tel = document.getElementById('telId').value;
    user.password = document.getElementById('passId').value;
    // console.log(user);
    document.getElementById('modal').classList.add('is-active');
    document.getElementById('modal-card-body').innerHTML = `${document.getElementById('usernameId').value} عزیز ثبت نام شما با موفقیت انجام شد `;
    resetForm();
    gCaptcha();
  }
}
function closeModal() {
  document.getElementById('modal').classList.remove('is-active');
}
document.getElementById('checkValidation').onclick = () => { checkValidation(); };
document.getElementById('closeModal').onclick = () => { closeModal(); };
document.getElementById('resetForm').onclick = () => { resetForm(); };
